rogue : room.o game.o rogue.o
	cc -g -o bin/rogue -lncurses bin/rogue.o bin/game.o bin/room.o

room.o : 
	cc -c -g src/room.cc -o bin/room.o

game.o : 
	cc -c -g src/game.cc -o bin/game.o

rogue.o :   
	cc -c -g src/rogue.cc -o bin/rogue.o

clean :
	rm bin/*.o ./bin/rogue
