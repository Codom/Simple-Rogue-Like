/* Author: Christopher Odom
 * Purpose: Cascade all data 
 * structures used in the game
 * under a single handler. 
 */
#include"rogue.h"


Game::Game()
{
	this->rooms[0];
	this->rooms[0].posx = 0;
	this->rooms[0].posy = 1;
	for(unsigned int i = 1; i < ROOMS - 1; i++)
	{
		//pick a number between 0 and 4
		int direction = 0;
		this->rooms[i];
		this->rooms[i+1];
		this->rooms[i].addneighbor(&this->rooms[i+1], direction);
		this->rooms[i+1].addneighbor(&this->rooms[i], (direction+2)%4);
		rooms[i].setneighborposition();
	}
}

void Game::drawrooms()
{
	Room * pRoom = rooms;
	for(int i = 0; i < ROOMS; i++)
	{
		pRoom->draw_room();
		pRoom++;
	}
}


