#ifndef ROGUE_H
#define ROGUE_H

#include<ncurses.h>
#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<vector>

#define ROOMS 15
#define ROOM_DIS 10
#define ROOM_VAR 15
#define NBORS 4

struct Player
{
	int posx, posy;
};

struct Mob
{
	int posx, posy;
	int strength, health;

};

struct Room
{
	int sizex, sizey;
	int posx, posy;
	Room();
	Room * neighbors[NBORS];
	void addneighbor(Room * neighbor, int direction);
	void setneighborposition();
	void draw_room();
	void drawhalls();
};


struct Game
{
private:
	//TODO: make this a dynamic array, or a vector
	Room rooms[ROOMS];
public:
	Game();
	void drawrooms();
};

void game_init(Game *mygame);

#endif
