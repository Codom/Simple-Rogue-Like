/* Author: Christopher Odom
 * 
 * Rooms are treated as a graph-like
 * data structure. Rooms have
 * no exact position, however they
 * store the address of 
 * neighboring rooms.
 */
#include"rogue.h"

Room::Room()
{
	this->sizex = rand() % 20 + 3;
	this->sizey = rand() % 10 + 3;
	this->posx = 0;
	this->posy = 0;	//Sentinel value
	for(int i = 0; i<NBORS; i++)
	{
		neighbors[i] = NULL;
	}
}

void Room::addneighbor(Room * neighbor, int direction)
{
	this->neighbors[direction] = neighbor;
}

void Room::setneighborposition()
{
	//This is messy shit. Find a more tasteful way
	for(unsigned int i = 0; i < NBORS; i++)
	{
		Room* nRoom = neighbors[i];
		if(nRoom && nRoom->posx == 0)
		{
			switch(i)
			{
			case 0:
				nRoom->posx = this->posx+this->sizex+
					(rand()%ROOM_DIS + 1);
				nRoom->posy += rand()%ROOM_VAR + 1;
				break;
			case 1:
				nRoom->posy = this->posy+this->sizey-
					(rand()%ROOM_DIS + 1);
				nRoom->posx += rand()%ROOM_VAR + 1;
				break;
			case 2:
				nRoom->posx = this->posx-this->sizex-
					(rand()%ROOM_DIS + 1);
				nRoom->posy -= rand()%ROOM_VAR + 1;
				break;
			case 3:
				nRoom->posy = this->posy-this->sizey-
					(rand()%ROOM_DIS + 1);
				nRoom->posx -= rand()%ROOM_VAR + 1;
				break;
			default: break;
			}
			nRoom->setneighborposition();
		}
	}
}

void Room::draw_room()
{
	int cx = this->posx;
	int cy = this->posy;
	for(int j = 0; j<+this->sizex; j++)
	{
		mvaddch(cy, cx+j, '+');
		mvaddch(cy+this->sizey, cx+j, '+');
	}
	for(int j = 0; j < this->sizey; j++)
	{
		mvaddch(cy+j, cx, '+');
		mvaddch(cy+j, cx+this->sizex, '+');
	}
}

void Room::drawhalls()
{
}

