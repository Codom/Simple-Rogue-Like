/**************************************************************
 * Author: Christopher Odom
 * 
 * This file implements the environment of the game. It 
 * directly handles I/O and implements the main loop
 * of the game. 
 *
 * ***********************************************************/
#include"rogue.h"

int main()
{
	//preseed the random number generator
	srand(time(NULL));
	Game mygame;
	game_init(&mygame);
	return 0;
}


void game_init(Game *mygame)
{
	int maxx, maxy;
	int keypress;

	initscr();
	//Read characters without a break
	cbreak();
	noecho();

	Player player{1, 1};

	//main loop
	for(;;)
	{
		//clear the screen of the previous content 
		clear();
		getmaxyx(stdscr, maxy, maxx);

		//Draw entities onto the screen
		mygame->drawrooms();
		mvaddch(player.posy, player.posx, '@');
		mvprintw(0,0,"(%d, %d) is the current player position"
			, player.posx, player.posy);

		
		//get input from keyboard
		keypress = getch();

		//run the keypress against the keymap
		switch(keypress){
			case 'w':
				if(player.posy > 1)
					player.posy--;
				break;
			case 's': 
				if(player.posy < maxy - 1)
					player.posy++;
				break;
			case 'a':
				if(player.posx > 0)
					player.posx--;
				break;
			case 'd': 
				if(player.posx < maxx - 1)
					player.posx++;
				break;
		}

		refresh();
	}
	getch();
	endwin();
}

